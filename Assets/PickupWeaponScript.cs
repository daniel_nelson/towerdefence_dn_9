﻿using UnityEngine;
using System.Collections;

public class PickupWeaponScript : MonoBehaviour {

	bool canPickUp = false;

	GameObject player;

	public GameObject weapon;
	
	// Update is called once per frame
	void Update () {
		if (canPickUp) {
			if (Input.GetKeyDown (KeyCode.F)) {
				if (player != null) {
					//Spawn the weapon as a child of the MainCamera (otherwise, the weapon doesn't follow the camera).
					GameObject mainCamera = player.transform.GetChild (0).gameObject;
					GameObject weaponObject = (GameObject)Instantiate (weapon, mainCamera.transform.position, mainCamera.transform.rotation);
					weaponObject.transform.parent = mainCamera.transform;
					Destroy (this.gameObject);

				}
			}
		}

	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			player = other.gameObject;
			canPickUp = true;
		}
	}
	void OnTriggerExit(Collider other){
		if(other.tag == "Player")
			canPickUp = false;
	}
}
