﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	public enum GameState { PHASE_BUILDING, PHASE_FIGHTING, PAUSED };

	public GameState state = GameState.PHASE_BUILDING;
	bool firstPhase = true;

	public float buildTime = 90f;
	public float nextPhaseCountdown = 0;
	public float defaultNextPhaseCountdown = 1;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("GameState: " + state);
		//if its the first phase of the level and enter is pressed then transition into standard fight - build phases
		if (state == GameState.PHASE_BUILDING &&
		    Input.GetKeyDown (KeyCode.Return) &&
		    firstPhase) {
			nextPhaseCountdown = defaultNextPhaseCountdown;
			firstPhase = false;
		} 
		if (state == GameState.PHASE_BUILDING && !firstPhase){
			if (nextPhaseCountdown > 5 && Input.GetKeyDown (KeyCode.Return)) {
				nextPhaseCountdown = 5;
			}
			if (nextPhaseCountdown > 0) {
				Debug.Log ("Time till next phase: " + nextPhaseCountdown);
				nextPhaseCountdown -= Time.deltaTime;
			}else if (nextPhaseCountdown <= 0) { 
				state = GameState.PHASE_FIGHTING;
			}
		}
	}


}
